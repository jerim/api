const express = require('express')
const app = express()
require('./config/db')
const bodyParser = require('body-parser')
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

const router = require('./routes.js')
const router2 = require('./routes2.js') // mongodb

// nuestra ruta irá en http://localhost:8080/api
// es bueno que haya un prefijo, sobre todo por el tema de versiones de la API
app.use('/api', router)
app.use('/api2', router2) // mongodb
const port = process.env.PORT || 8080 // establecemos nuestro puerto y devuelve el 8080. Siempre por defecto

// app.get('/', (req, res) => {
//   res.json({ mensaje: '¡Hola Mundo!' })
// })

// iniciamos nuestro servidor
app.listen(port, () => {
  console.log('API escuchando en el puerto ' + port)
})
