const express = require('express')
const router = express.Router()
const productController = require('../../controllers/v2/productController.js')
const servicejwt = require('../../service/servicejwt.js')
const moment = require('moment')
const auth = require('../../middlewares/auth.js')

router.use(auth.auth)

router.get('/', (req, res) => {
  console.log(' en el ruta de productos')
  productController.index(req, res)
})
router.post('/', (req, res) => {
  console.log(' en el ruta create')
  productController.create(req, res)
})

router.put('/:id', (req, res) => {
  console.log(' en el ruta update')
  productController.update(req, res)
})

router.get('/:id', (req, res) => {
  console.log(' en el ruta show')
  productController.show(req, res)
})

router.delete('/:id', (req, res) => {
  console.log(' en el ruta destroy')
  productController.destroy(req, res)
})

module.exports = router // exportamos
