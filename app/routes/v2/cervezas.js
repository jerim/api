const express = require('express')
const router = express.Router()
const cervezaController = require('../../controllers/v2/cervezaController.js')
// establecemos nuestra primera ruta, mediante get.
router.get('/', (req, res) => {
  console.log(' en el ruta cervezas')
  cervezaController.index(req, res)
})

router.get('/search', (req, res) => {
  console.log(' en el ruta search')
  cervezaController.search(req, res)
})

router.get('/:id', (req, res) => {
  console.log(' en el ruta show')
  cervezaController.show(req, res)
})

module.exports = router // exportamos
