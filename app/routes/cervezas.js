const express = require('express')
const router = express.Router()
const cervezaController = require('../controllers/cervezaController')
// establecemos nuestra primera ruta, mediante get.
router.get('/', (req, res) => {
  console.log(' en el ruta index de cervezas')
  cervezaController.index(req, res)
})

router.get('/cervezas/:id', (req, res) => {
  cervezaController.show(req, res)
})

/* router.get('/cervezas', (req, res) => {
  res.json({ mensaje: '¡A beber cerveza!' })
}) */

router.post('/cervezas', (req, res) => {
  res.json({ mensaje: 'Cerveza guardada' })
  cervezaController.store(req, res)
})

router.delete('/cervezas/:id', (req, res) => {
  res.json({ mensaje: 'Cerveza borrada' })
  cervezaController.destroy(req, res)
})

router.put('/cervezas/:id', (req, res) => {
  res.json({ mensaje: 'Cerveza actualizada' })
  cervezaController.update(req, res)
})

module.exports = router
