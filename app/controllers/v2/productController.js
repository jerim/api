const Product = require('../../models/v2/Product') // llama al modelo Producto

const index = (req, res) => {
  Product.find((err, products) => {
    // find no se le pasa nada , es como un select sino hay argumento pasa todo
    if (err) {
      return res.status(500).json({
        message: 'Error obteniendo el producto'
      })
    }
    return res.json(products)
  })
}

const create = (req, res) => {
  const Products = new Product()
  Products.name = req.body.name
  Products.price = req.body.price
  Products.description = req.body.description

  Products.save((err, products) => {
    if (err) {
      return res.status(400).json({
        message: 'Error al guardar el producto',
        error: err
      })
    }
    return res.status(201).json(products)
  })
}

const { ObjectId } = require('mongodb') // 12 bytes
const show = (req, res) => {
  const id = req.params.id
  Product.findOne({ _id: id }, (err, products) => {
    if (!ObjectId.isValid(id)) {
      return res.status(404).json({ mensaje: 'id no valido' })
    }
    if (err) {
      return res.status(500).json({
        message: 'Se ha producido un error al obtener el producto'
      })
    }
    if (!products) {
      return res.status(404).json({
        message: 'No tenemos este producto'
      })
    }
    return res.json(products)
  })
}

const update = (req, res) => {
  const id = req.params.id // parametros de la ruta entre :
  Product.findById({ _id: id }, (err, products) => {
    // busqueda por id
    if (!ObjectId.isValid(id)) {
      return res.status(404).send()
    }
    if (err) {
      return res.status(500).json({
        message: 'Se ha producido un error al guardar el producto',
        error: err
      })
    }

    if (!products) {
      return res.status(404).json({
        message: 'No hemos encontrado el producto'
      })
    }

    Object.assign(products, req.body)

    products.save((err, products) => {
      if (err) {
        return res.status(500).json({
          message: 'Error al guardar el producto'
        })
      }
      if (!products) {
        return res.status(404).json({
          message: 'No hemos encontrado el producto'
        })
      }
      return res.json(products)
    })
  })
}
const destroy = (req, res) => {
  const id = req.params.id
  Product.findByIdAndDelete(id, (err, data) => {
    if (err) return res.status(500).json({ message: 'error' })
    return res.json(data)
  })
}
module.exports = {
  index,
  create,
  show,
  update,
  destroy
}
