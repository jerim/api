const connection = require('../config/dbconection')
const Cerveza = require('../models/Cerveza')
const index = (req, res) => {
  const cervezas = Cerveza.index(function (status, data, fields) {
    res.status(status).json(data)
  })
  // (console.log(cervezas)
  // return "lista de cerzas desde el controlador !!";
}

const show = (req, res) => {
  console.log('detalle show')
  const id = req.params.id
  const cervezas = Cerveza.find(id, function (status, data) {
    if (err) {
      res.status(500).json({ mensaje: 'error' })
    } else {
      res.status(201).json(data)
    }
  })
}
const store = (req, res) => {
  console.log('crear nueva cerveza')

  const cerveza = {
    id: req.body.id,
    name: req.body.name,
    container: req.body.container,
    price: req.body.price,
    alcohol: req.body.alcohol
  }
  Cerveza.create(cerveza, (err, data) => {
    if (err) {
      res.status(500).json({ mensaje: 'fallo la insercion' })
    } else {
      res.status(201).json(data)
    }
  })

  console.log('nueva cerveza')
}

const update = (rep, res) => {
  const cerveza = {
    id: req.body.id,
    name: req.body.name,
    container: req.body.container,
    price: req.body.price,
    alcohol: req.body.alcohol
  }

  Cerveza.update(cerveza, (err, data) => {
    if (err) {
      res.status(500).json({ mensaje: 'fallo la insercion' })
    } else {
      res.status(201).json(data)
    }
  })
}

const destroy = (rep, res) => {
  const id = req.params.id
  const sql = 'DELETE FROM cervezas WHERE id = ?'
  cerveza.destroy(id, (err, data) => {
    if (err) {
      res.status(500).json({ mensaje: 'error' })
    } else {
      res.status(201).json({ mensaje: 'borrado' })
    }
  })
}

module.exports = {
  index,
  show,
  store,
  update,
  destroy
}
