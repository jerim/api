const express = require('express')
const router = express.Router()
const routerCervezas = express.Router('./routes/cervezas.js')

// establecemos nuestra primera ruta, mediante get.
router.get('/', (req, res) => {
  res.json({ mensaje: '¡Bienvenido a nuestra API!' })
})
router.post('/', (req, res) => {
  res.json({ mensaje: 'Cerveza guardada' })
})

router.get('/', (req, res) => {
  res.json({ mensaje: '¡A beber cerveza!' })
})

router.delete('/', (req, res) => {
  res.json({ mensaje: 'Cerveza borrada' })
})

router.put('/', (req, res) => {
  res.json({ mensaje: 'Cerveza actualizada' })
})

router.use('/cervezas', routerCervezas)

module.exports = router
