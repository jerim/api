const express = require('express')
const router = express.Router()
const routerCervezas = require('./routes/v2/cervezas.js')
const routerProductos = require('./routes/v2/products.js')
const routerUsers = require('./routes/v2/users.js')
// establecemos nuestra primera ruta, mediante get.
router.get('/', (req, res) => {
  res.json({ mensaje: '¡Bienvenido a nuestra API con Mongodb!' })
})
/* router.get('/cervezas', (req, res) => {
  res.json({ mensaje: 'ruta de cervezas' })
}) */
// para la ruta de prueba (modelo) mongoose || va con ambar

// const Cerveza = require('./models/v2/Cerveza')
// router.get('/ambar', (req, res) => {
//   const miCerveza = new Cerveza({ nombre: 'Ambar' })
//   miCerveza.save((err, miCerveza) => {
//     if (err) return console.error(err)
//     console.log(`Guardada en bbdd ${miCerveza.nombre}`)
//   })
// })

// fin prueba ambar

// enganchadas a partir de aqui la ruta de cervezas
router.use('/cervezas', routerCervezas)
router.use('/products', routerProductos)
router.use('/users', routerUsers)
module.exports = router
