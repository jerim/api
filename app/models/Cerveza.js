const connection = require('../config/dbconection')

const index = function(callback) {
  console.log('Lista de cervezas')
  const sql = 'SELECT * FROM cervezas'
  connection.query(sql, (err, result, fields) => {
    if (err) {
      console.log(err)
      callback(500, err)
    } else {
      console.log(result)
      callback(200, result, fields)
    }
  })
}

const find = function(id, callback) {
  const sql = 'SELECT * from cervezas WHERE id = ?'
  const id = req.params.id
  connection.query(sql, [id], (err, result, fields) => {
    if (err) {
      console.log(err)
      callback(500, err)
    } else {
      console.log(result)
      callback(200, result, fields)
    }
  })
}

const create = function(cerveza, callback) {
  const sql =
    'INSERT INTO cervezas(name,container,alcohol,price' + 'VALUES(?,?,?,?)'
  connection.query(
    sql,
    [cerveza.name, cerveza.container, cerveza.alcohol, cerveza.price],
    (err, result) => {
      if (err) {
        callback(500, result)
      } else {
        callback(200, result)
      }
    }
  )
}

const update = (cerveza, callback) => {
  const sql = 'UPDATE cervezas Set name=?, container=? WHERE id=?'
  connection.query(
    sql,
    [cerveza.name, cerveza.container, cerveza.alcohol, cerveza.price],
    (err, result) => {
      if (err) {
        callback(500, result)
      } else {
        callback(200, result)
      }
    }
  )
}

const destroy = (cervideza, callback) => {
  const sql = 'DELETE FROM cervezas WHERE id = ?'
  connection.query(sql, [id], (err, result, fields) => {
    if (err) {
      console.log(err)
      callback(500, err)
    } else {
      console.log(result)
      callback(200, result, fields)
    }
  })
}
module.exports = {
  index,
  find,
  create,
  update,
  destroy
}
